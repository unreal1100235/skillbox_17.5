// SkillBox_17.5.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
class Vector
{
private:
	double x, y, z;
public:
	Vector() : x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void ShowVector()
	{
		std::cout << x << " " << y << " " << z << "\n";
	}
	double VectorModule(Vector)
	{		
		return sqrt((x * x) + (y * y) + (z * z));
	}
};


int main()
{
	Vector a(5, 5, 5);
	a.ShowVector();
	std::cout << a.VectorModule(a) << "\n";
}

